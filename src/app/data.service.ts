import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { backupFile } from './home/home.component';

@Injectable({
  providedIn: 'root'
})
export class DataService {
  constructor(private _http: HttpClient) { }

  // grab the data from the API
  getData(){
    return this._http.get<backupFile[]>('http://192.168.1.92:8080/api/files')
  }
}