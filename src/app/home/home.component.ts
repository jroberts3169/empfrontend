import { Component, OnInit } from '@angular/core';
import { DataService } from '../data.service';

export interface backupFile {
  fileName: string,
  fileSize: string,
  transmitTime: string,
  receiveTime: string,
  data: nasaJSON;
}

export interface nasaJSON{
  name: string,
  id: string,
  nametype: string,
  recclass: string,
  mass: string,
  fall: string,
  year: string,
  reclat: string,
  reclong: string
}

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  displayedColumns: string[] = ['fileName', 'fileSize', 'transmitTime', 'receiveTime'];
  public files: backupFile[];
  interval: any;

  constructor(private data: DataService) {
    this.files = [];
  }

  ngOnInit() {
    this.getData();
    
    this.interval = setInterval(() => { 
      this.getData(); 
    }, 5000);

  }

  getData(){
    this.data.getData().subscribe((data: backupFile[]) => {
      this.files = data;
    });
  }
}
